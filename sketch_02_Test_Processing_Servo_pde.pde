/*
Project : LaserGun V 0.1 
Page: http://jungletronics.blogspot.com.br/2014/06/lasergun-project.html
YouTube: http://youtu.be/andpnW46vQE
Code: https://bitbucket.org/giljr/lasergunproject
File : sketch_02_Test_Processing_Servo

this aims initial tests the operation of the laser in Processing 
with Arduino communicating via Firmata library; In Arduino load 
Examples > Firmata > StandardFirmata.ino; Please import on Processing 
Sketch > Import Library... > Add Library > Arduino (Firmata)

P.Velho - Brazil
giljr.2009@gmail.com (Gil Jr)
Date: 29/06/2014
*/

import processing.serial.*;
import cc.arduino.*;
Arduino arduino;

int servoPin = 10; // Control pin for servo motor

void setup() {

  size (180, 50);
  arduino = new Arduino(this, Arduino.list()[0]);
  arduino.pinMode(servoPin, Arduino.OUTPUT);
  // note - we are setting a digital pin to output
}

void draw() {
  arduino.analogWrite(servoPin, mouseX); // the servo moves to the horizontal location of the mouse
}
